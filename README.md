# Analytics tool for Innometrics

### Install project

1. clone repo `git clone --recursive git@gitlab.com:ihar_shulhan/innometrics-analytics-tool.git`
2. install node modules `npm install`

### Lint project
1. run `npm run lint`
2. correct all errors before pushing

