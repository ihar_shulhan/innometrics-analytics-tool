/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('authtoken_token', {
    key: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    created: {
      type: DataTypes.TIME,
      allowNull: false,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'auth_user',
        key: 'id',
      },
      unique: true,
    },
  }, {
    tableName: 'authtoken_token',
  });
};
