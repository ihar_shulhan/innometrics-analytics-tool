/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('dash_dashboardplugin', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    plugin_uid: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
  }, {
    tableName: 'dash_dashboardplugin',
  });
};
