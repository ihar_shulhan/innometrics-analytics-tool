/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Measurement', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    tableName: 'measurements',
    classMethods: {
      associate(models) {
        this.belongsTo(models.Activity, {
          as: 'measurements',
          foreignKey: 'activity_id',
        });
        this.belongsTo(models.Activity, {
          as: 'activityDate',
          foreignKey: 'activity_id',
        });
      },
    },
  });
};
