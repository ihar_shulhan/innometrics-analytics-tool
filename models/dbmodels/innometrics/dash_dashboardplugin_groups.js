/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('dash_dashboardplugin_groups', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    dashboardplugin_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'dash_dashboardplugin',
        key: 'id',
      },
      unique: true,
    },
    group_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'auth_group',
        key: 'id',
      },
    },
  }, {
    tableName: 'dash_dashboardplugin_groups',
  });
};
