/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Activity', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    comments: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'user_id',
      references: {
        model: 'auth_user',
        key: 'id',
      },
    },
  }, {
    tableName: 'activities_activity',
    classMethods: {
      associate(models) {
        this.hasMany(models.Measurement, {
          foreignKey: 'activity_id',
          as: 'activityDate',
        });
        this.hasMany(models.Measurement, {
          foreignKey: 'activity_id',
          as: 'measurements',
        });
      },
    },
  });
};
