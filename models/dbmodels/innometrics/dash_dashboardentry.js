/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('dash_dashboardentry', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    layout_uid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    placeholder_uid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    plugin_uid: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    plugin_data: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    position: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'auth_user',
        key: 'id',
      },
    },
    workspace_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'dash_dashboardworkspace',
        key: 'id',
      },
    },
  }, {
    tableName: 'dash_dashboardentry',
  });
};
