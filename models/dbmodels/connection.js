import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

/**
 * Connect to database and import all models to Sequelize
 * @param  {object} config          Configuration object
 * @param  {string} config.host     Host where db server lives
 * @param  {string} config.port     Port on which db server runs
 * @param  {string} config.db       Database name
 * @param  {string} config.dialect  Database management system
 * @param  {string} config.username Username for db connection
 * @param  {string} config.password Password for db connection
 * @return {Object}          Object containing all models
 */
const connection = (config, pathToModels) => {
  const { host, port, db, dialect, username, password } = config;

  /**
   * Path to models folder
   * @const
   * @type {string}
   */
  const MODELS_PATH = path.join(__dirname, pathToModels);
  const sequelize = new Sequelize(db, username, password, {
    host,
    dialect,
    port,
    logging: false,
    define: {
      timestamps: false,
    },
  });
  const models = {};

  fs.readdirSync(MODELS_PATH).forEach((file) => {
    const model = sequelize.import(path.join(MODELS_PATH, file));
    models[model.name] = model;
  });

  Object.entries(models).forEach((item) => {
    const model = item[1];
    if (typeof model.associate === 'function') {
      model.associate(models);
    }
  });

  return {
    sequelize,
    models,
  };
};

export default connection;
