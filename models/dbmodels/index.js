import config from 'config';
import Connection from './connection';

const innometricsData = config.get('innometrics');
export const db = new Connection(innometricsData, 'innometrics');
