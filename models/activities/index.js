import sequelize from 'sequelize';
import moment from 'moment';
import { db } from '../dbmodels';

const { models } = db;

export const getActivities = (start = moment().subtract(50, 'days').toDate(), end = moment().toDate()) => models.Activity.findAll({
  include: [{
    model: models.Measurement,
    as: 'activityDate',
    attributes: [],
    where: {
      name: 'from',
      value: {
        $gte: start,
        $lte: end,
      },
    },
  }, {
    model: models.Measurement,
    as: 'measurements',
  }],
});

export const getActivitiesByUser = (userIds, start = moment().subtract(50, 'days').toDate(), end = moment().toDate()) => models.Activity.findAll({
  where: {
    userId: {
      $in: userIds,
    },
  },
  include: [{
    model: models.Measurement,
    as: 'activityDate',
    attributes: [],
    where: {
      name: 'from',
      value: {
        $gte: start,
        $lte: end,
      },
    },
  }, {
    model: models.Measurement,
    as: 'measurements',
  }],
});

export const getActivitiesTop = (start = moment().subtract(50, 'days').toDate(), end = moment().toDate()) => models.Activity.findAll({
  include: [{
    model: models.Measurement,
    attributes: [],
    as: 'activityDate',
    where: {
      name: 'from',
      value: {
        $gte: start,
        $lte: end,
      },
    },
  }, {
    model: models.Measurement,
    as: 'measurements',
    attributes: [],
    where: {
      name: 'duration',
    },
  }],
  attributes: ['Activity.name', [sequelize.fn('SUM', sequelize.literal('cast("measurements"."value" as real)')), 'activityDuration']],
  order: [
    sequelize.literal('"activityDuration" DESC'),
  ],
  group: ['Activity.name'],
  raw: true,
});

export const getActivitiesTopByUser = (userIds, start = moment().subtract(50, 'days').toDate(), end = moment().toDate()) => models.Activity.findAll({
  where: {
    userId: {
      $in: userIds,
    },
  },
  include: [{
    model: models.Measurement,
    attributes: [],
    as: 'activityDate',
    where: {
      name: 'from',
      value: {
        $gte: start,
        $lte: end,
      },
    },
  }, {
    model: models.Measurement,
    as: 'measurements',
    attributes: [],
    where: {
      name: 'duration',
    },
  }],
  attributes: ['Activity.name', [sequelize.fn('SUM', sequelize.literal('cast("measurements"."value" as real)')), 'activityDuration']],
  order: [
    sequelize.literal('"activityDuration" DESC'),
  ],
  group: ['Activity.name'],
  raw: true,
});

