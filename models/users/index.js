import moment from 'moment';
import { db } from '../dbmodels';

const { models } = db;

export const addNewUser = (email, password) => {
  if (email && password) {
    return models.User.create({
      email,
      password,
      username: email,
      first_name: email,
      last_name: email,
      is_active: true,
      is_staff: false,
      is_superuser: false,
      date_joined: moment().toDate(),
    });
  }
  return Promise.reject(new Error('Username or password is not specified'));
};

export const findUserByEmail = email => models.User.findOne({ where: { email } });


export const getUserById = id => models.User.findOne({ where: { id } });

