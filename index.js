import express from 'express';
import cookieParser from 'cookie-parser';
import passport from 'passport';
import session from 'express-session';
import config from 'config';
import bodyParser from 'body-parser';
import cors from 'cors';
import middlewares from './middlewares';
import controllers from './controllers';
import { db } from './models/dbmodels';

const app = express();

const PORT = process.env.PORT || 8080;        // set our port
app.use('*', cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bodyParser.json());

app.use(session(config.get('expressSession')));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', middlewares);
app.use('/', controllers);

Promise.all([db.sequelize.sync()]).then(() => {
  app.listen(PORT, () => console.log( // eslint-disable-line no-console
    `Server is now running on http://localhost:${PORT}`
  ));
});
