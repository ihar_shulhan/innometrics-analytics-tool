import express from 'express';
import activities from './activities';
import auth from './auth';

const router = new express.Router({ mergeParams: true });

router.use('/api/activities', activities);
router.use('/auth', auth);

export default router;
