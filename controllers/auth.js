import express from 'express';
import passport from 'passport';
import passportLocal from 'passport-local';
import pbkdf2 from 'pbkdf2-sha256';
import crypto from 'crypto';

import { addNewUser, findUserByEmail, getUserById } from '../models/users';

const LocalStrategy = passportLocal.Strategy;

const router = new express.Router({ mergeParams: true });

const LEN = 32;
const SALT_LEN = 12;
const ITERATIONS = 30000;

const hashPassword = (password, salt) => new Promise((resolve, reject) => {
  if (salt && salt.length === SALT_LEN) {
    const hash = pbkdf2(password, new Buffer(salt), ITERATIONS, LEN).toString('base64');

    resolve(`pbkdf2_sha256$30000$${salt}$${hash}`);
  } else {
    crypto.randomBytes(SALT_LEN / 2, (err, newSalt) => {
      if (err) {
        reject(err);
      }
      const newSaltS = newSalt.toString('hex');
      const hash = pbkdf2(password, new Buffer(newSaltS), ITERATIONS, LEN).toString('base64');

      resolve(`pbkdf2_sha256$30000$${newSaltS}$${hash}`);
    });
  }
});

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  getUserById(id)
    .then((user) => {
      if (user === null) {
        done(null, false);
      } else {
        done(null, user);
      }
    }).catch(e => done(e));
});

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
},
  (email, password, done) => {
    findUserByEmail(email)
      .then((user) => {
        if (user === null) {
          done(null, false, 'User not found');
        } else {
          hashPassword(password, user.password.substring(20, 32)).then((hash) => {
            if (user.password === hash) {
              done(null, user);
            } else {
              done(null, false, 'Incorrect password');
            }
          });
        }
      }).catch(e => done(e));
  }
));

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      return res.status(401).json({
        err: info,
        sessionId: req.session.id,
      });
    }
    return req.logIn(user, (error) => {
      if (error) {
        return res.status(500).json({
          err: 'Could not log user in',
          sessionId: req.session.id,
        });
      }
      return res.status(200).json({
        msg: 'Login success!!',
        sessionId: req.session.id,
        userId: user.id,
      });
    });
  })(req, res, next);
});

router.post('/register',
  (req, res) => {
    hashPassword(req.body.password)
    .then(password => addNewUser(req.body.email, password))
    .then(() => res.status(200).json({
      msg: 'Registered!',
      sessionId: req.session.id,
    }))
    .catch(err => res.status(500).json({
      msg: 'Error at adding local user',
      err: err.message,
    }));
  }
);

const loginRequired = (req, res, next) => {
  if (!req.user) return res.status(401).json({ status: 'Please log in' });
  return next();
};

router.get('/logout', loginRequired, (req, res) => {
  req.logout();
  return res.status(200).json({
    msg: 'Bye!',
  });
});

export default router;
