import express from 'express';
import { getActivities, getActivitiesByUser, getActivitiesTop, getActivitiesTopByUser } from '../models/activities';

const router = new express.Router({ mergeParams: true });

router.get('/', (req, res, next) => {
  const { userIds } = req.query;
  const user = req.user;

  if (!user || user.is_superuser) {
    return next();
  }

  let users = [];
  try {
    users = JSON.parse(userIds);
  } catch (e) {
    return res.status(400).json({
      msg: 'User array is invalid',
      data: userIds,
    });
  }

  if (!userIds || users.length !== 1 || users[0] !== user.id) {
    return res.status(400).json({
      msg: 'Don\'t have access',
    });
  }
  return next();
});


router.get('/', (req, res) => {
  const { userIds, start, end } = req.query;
  let query;

  if (userIds) {
    let users = [];
    try {
      users = JSON.parse(userIds);
    } catch (e) {
      return res.status(400).json({
        msg: 'User array is invalid',
        data: userIds,
      });
    }
    query = getActivitiesByUser(users, start, end);
  } else {
    query = getActivities(start, end);
  }
  return query
    .then(data => res.json({
      msg: 'success',
      data,
    }))
    .catch(err => res.status(500).json({
      msg: 'Internal error',
      err: err.message,
    }));
});

router.get('/top', (req, res) => {
  const { userIds, start, end } = req.query;
  let query;

  if (userIds) {
    let users = [];
    try {
      users = JSON.parse(userIds);
    } catch (e) {
      return res.status(400).json({
        msg: 'User array is invalid',
        data: userIds,
      });
    }
    query = getActivitiesTopByUser(users, start, end);
  } else {
    query = getActivitiesTop(start, end);
  }
  return query
  .then(data => res.json({
    msg: 'success',
    data,
  }))
  .catch(err => res.status(500).json({
    msg: 'Internal error',
    err: err.message,
  }));
});

export default router;
