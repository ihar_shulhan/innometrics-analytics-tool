import express from 'express';

const router = new express.Router();

router.use('/api', (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.status(401).json({
      err: 'Please login if you want my secret!',
      sessionId: req.session.id,
    });
  }
  return next();
});

export default router;
